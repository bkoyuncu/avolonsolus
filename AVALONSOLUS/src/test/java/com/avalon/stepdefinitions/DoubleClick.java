package com.avalon.stepdefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DoubleClick {

	@Given("User get on https:\\/\\/testautomationu.applitools.com\\/cucumber-javascript-tutorial\\/")
	public void user_get_on_https_testautomationu_applitools_com_cucumber_javascript_tutorial() {
		
		Driver.getDriver().get(ConfigurationReader.getPropertiesFileValue("cucurl"));
		
	}
	
	@When("User performing DoubleClick on video")
	public void user_performing_DoubleClick_on_video() {
		
		Driver.getDriver().switchTo().frame(0);
		
		WebElement video=Driver.getDriver().findElement(By.xpath("//button[@class='ytp-large-play-button ytp-button']"));
		
		Actions act=new Actions(Driver.getDriver());
		act.moveToElement(video).doubleClick().build().perform();
		
		
		
	}
	
	@Then("User should see video in fullscreen")
	public void user_should_see_video_in_fullscreen() {
		
		
		WebElement fullScreen=Driver.getDriver().findElement(By.xpath("//button[@class='ytp-fullscreen-button ytp-button']"));
		
		if(fullScreen!=null) {
			
			System.out.println("DoubleClick successful");
			
		}else {
			
			System.out.println("DoubleClick FAILED");
		}
		
		
		
		
	}

}
