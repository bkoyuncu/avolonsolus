@regression
Feature: Multiple Window Handling

  Scenario: Handle multiple window
    Given User should open Chrome
    When User navigates to URL "http://www.naukri.com"
    And User should be able to see multi window page
    Then User should close all windows
    And User closes main browser
