package com.avalon.stepdefinitions;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class HandlingMultipleWindows {

	WebDriver driver;

	@Given("User should open Chrome")
	public void user_should_open_Chrome() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@When("User navigates to URL {string}")
	public void user_navigates_to_URL(String url) {
		driver.get(url);

	}

	@When("User should be able to see multi window page")
	public void user_should_be_able_to_see_multi_window_page() {

	}

	@Then("User should close all windows")
	public void user_should_close_all_windows() {
		String mainWindow = driver.getWindowHandle();
		Set<String> set = driver.getWindowHandles();
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				driver.close();
			}
		}

		driver.switchTo().window(mainWindow);
	}

	@Then("User closes main browser")
	public void user_closes_main_browser() {
		driver.close();
	}

}
