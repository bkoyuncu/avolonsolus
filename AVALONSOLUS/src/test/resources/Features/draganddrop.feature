@regression
Feature: Perform Drag and Drop

Scenario: Open https://www.w3schools.com/html/html5_draganddrop.asp and do draganddrop

Given User opern https://www.w3schools.com/html/html5_draganddrop.asp webpage
When User moves image from one window to another
Then User should see image in another window
