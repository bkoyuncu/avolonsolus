package com.avalon.stepdefinitions;

import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.Driver;

import io.cucumber.core.gherkin.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class AvolonHook {

	@Before
	public void driverSetUp() {
		
		Driver.startDriver();
		
	}
	
	
	@After
	public void takeScreenshotIfFailed(Scenario scenario) {
		
		if(scenario == null) {
			
		BrowseUtils.takeScreenshotMethod(Driver.getDriver());
		
		}
		
		Driver.closeDriver();
	}

}
