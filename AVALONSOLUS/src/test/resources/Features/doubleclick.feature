@regression
Feature: Perform DoubleClick

Scenario: Double click on video to make it FullScreen

Given User get on https://testautomationu.applitools.com/cucumber-javascript-tutorial/
When User performing DoubleClick on video
Then User should see video in fullscreen

