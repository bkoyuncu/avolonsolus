@regression
Feature: Sorting options on the seach result page

  Scenario Outline: 
    Given The user is on the "https://avalonsolus.com/courses/" page
    When User sorts by <option>
    Then Following products should display  on the top
      | name  | <name>  |
      | price | <price> |

    Examples: 
      | option          | name                                                                     | price |
      | Alphabetical    | 1-Hour WordPress Basic Tutorial                                          |    39 |
      | Most members    | Introduction LearnPress  LMS pluginIntroduction LearnPress  LMS plugin | free  |
      | Newly published | Introduction LearnPress  LMS pluginIntroduction LearnPress  LMS plugin | free  |
