package com.avalon.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(features = "src\\test\\resources\\Features", glue = {
		"src\\test\\java\\com\\avalon\\stepdefinitions" }, plugin = { "pretty",
				"json:target/cucumber-reports/Cucumber.json",
				"junit:target/cucumber-reports/Cucumber.xml" }, monochrome = true, dryRun = false, tags = {
						"@regression" })

public class CukesRunner {

}
