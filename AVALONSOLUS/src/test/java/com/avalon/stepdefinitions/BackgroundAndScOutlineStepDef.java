package com.avalon.stepdefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BackgroundAndScOutlineStepDef {
	

	@Given("The user is navigate to MainPage page")
	public void the_user_is_navigate_to_MainPage_page() {
		
		Driver.getDriver().get(ConfigurationReader.getPropertiesFileValue("url"));
		String expectedTitle=Driver.getDriver().getTitle();
		
		if(expectedTitle.equals(Driver.getDriver().getTitle())) {
			
			System.out.println("Verify title succeed");
			
		}else {
			System.out.println("Verify title FAILED!");
		}
		
		
	    
	}

	@When("User click LogIn button")
	public void user_click_LogIn_button() {
		
		WebElement logInButton=Driver.getDriver().findElement(By.xpath("//a[@class=\"login js-show-popup\"]"));
		JavascriptExecutor jse=(JavascriptExecutor) Driver.getDriver();
		jse.executeScript("arguments[0].click();", logInButton);
		
	   
	}

	@Then("User should see LogIn window")
	public void user_should_see_LogIn_window() {
		
		WebElement logInWindow=Driver.getDriver().findElement(By.xpath("//form[@name='loginpopopform']"));
		
		if(logInWindow!=null) {
			System.out.println("Enter your username and password");
		}else {
			System.out.println("LogIn Window Not Found!");
		}
		
		
	    
	}


	@When("User enters {string} and {string} and click LogIn button")
	public void user_enters_and_and_click_LogIn_button(String string, String string2) {
		
		WebElement usernm=Driver.getDriver().findElement(By.xpath("//input[@placeholder='Username or email']"));
		WebElement pass=Driver.getDriver().findElement(By.xpath("//input[@placeholder='Password']"));
		WebElement loginButton=Driver.getDriver().findElement(By.xpath("//input[@value='Login']"));
		
		JavascriptExecutor jse=(JavascriptExecutor) Driver.getDriver();
		
		jse.executeScript("arguments[0].value='"+string+"';", usernm);
		jse.executeScript("arguments[0].value='"+string2+"';", pass);
		jse.executeScript("arguments[0].click();", loginButton);
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	   
	}

	@Then("User should see You have logged in")
	public void user_should_see_You_have_logged_in() {
		
		WebElement logOutBoutton=Driver.getDriver().findElement(By.xpath("//a[@class='logout']"));
		
		if(logOutBoutton!=null) {
			
			System.out.println("LogIn successful");
			
		}else {
			System.out.println("LogIn FAILED!");
		}
		
	
		
		
	  
	}


}
