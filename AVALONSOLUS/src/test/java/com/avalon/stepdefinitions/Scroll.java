package com.avalon.stepdefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Scroll {

	@Given("User get to https:\\/\\/testautomationu.applitools.com\\/cucumber-javascript-tutorial\\/")
	public void user_get_to_https_testautomationu_applitools_com_cucumber_javascript_tutorial() {
		
		Driver.getDriver().get(ConfigurationReader.getPropertiesFileValue("cucurl"));
		
		
	}
	
	@When("User perform Scroll Down")
	public void user_perform_Scroll_Down() {
		
		WebElement scroller=Driver.getDriver().findElement(By.xpath("//div[@id='chapter-nav-scroller']"));
		
		Actions act=new Actions(Driver.getDriver());
		act.keyDown(scroller, Keys.CONTROL).sendKeys(scroller, Keys.END).build().perform();
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
		
		
		
	}
	
	@Then("User should see Chapter6")
	public void user_should_see_Chapter6() {
		
		WebElement chptrSix=Driver.getDriver().findElement(By.xpath("//*[@id=\"chapter-nav-scroller\"]/div[13]/a"));
		
		if(chptrSix!=null) {
			
			System.out.println("Scrolling successful");
		}else {
			
			System.out.println("Scrolling FAILED");
			
		}
		
		
	}
	
	


}
