@wip @regression
Feature: Background and Scenario Outline usage in framework

  Background: User is logged in
    Given The user is navigate to MainPage page
    When User click LogIn button
    Then User should see LogIn window

@sprint
  Scenario Outline: Log In to website
    When User enters "<Username>" and "<Password>" and click LogIn button
    Then User should see You have logged in

    Examples: 
      | Username    | Password      |
      | service_dev | IBecameACoder |
