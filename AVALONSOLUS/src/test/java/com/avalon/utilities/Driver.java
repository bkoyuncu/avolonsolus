package com.avalon.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Driver {

private static WebDriver driver;
	
	public static WebDriver getDriver() {
		
		return driver;
		
	}
	
	
	public static WebDriver startDriver() {
		
		if(driver==null) {
			
			switch (ConfigurationReader.getPropertiesFileValue("browser")) {
			
			case "chrome":
				WebDriverManager.chromedriver().setup();
				driver=new ChromeDriver();
				break;
				
			case "firefox":
				WebDriverManager.firefoxdriver().setup();
				driver= new FirefoxDriver();
				break;
			
			}
		}
			
		
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			
			
			return driver;
		}
	
	public static void closeDriver() {
		
		if(driver!=null) {
			
			driver.quit();
			driver=null;
		}
		
		
	}


}
