package com.avalon.utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BrowseUtils {

public static TakesScreenshot ts;
	
	public static void takeScreenshotMethod(WebDriver driver) {
		
		ts=((TakesScreenshot)Driver.getDriver());
		
		try {
		File file=ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file, new File("C:\\Users\\admin\\eclipse-workspace\\AVALONSOLUS\\target\\ScreenShots"+"\\FailureScreenShot"+".png"));
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		
		
	}

	
}


