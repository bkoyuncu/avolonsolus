package com.avalon.stepdefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class AssertAndVerify {

	@Given("User navigate on http:\\/\\/avalonsolus.com\\/")
	public void user_navigate_on_http_avalonsolus_com() {
		
		Driver.getDriver().get(ConfigurationReader.getPropertiesFileValue("url"));
		
	}
	
	@When("Use Assert and Verify")
	public void use_Assert_and_Verify() {
		
		String avalonTitle=Driver.getDriver().getTitle();
		System.out.println(avalonTitle);

		Assert.assertEquals(avalonTitle, "AVALON  One Stop Corporate Training Solutions");

	
		
	}
	
	@Then("User should see successful use of Assert and Verify")
	public void user_should_see_successful_use_of_Assert_and_Verify() {
		
		WebElement registrationWindow=Driver.getDriver().findElement(By.xpath("//div[@class='thim-register-now-form top-homepage']"));
		if(registrationWindow!=null) {
		System.out.println("Verify was successful");
		}else {
		System.out.println("Verify Failed");
		}
		
	}

	
}
