package com.avalon.stepdefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DragAndDrop {

	@Given("User opern https:\\/\\/www.w3schools.com\\/html\\/html5_draganddrop.asp webpage")
	public void user_opern_https_www_w3schools_com_html_html5_draganddrop_asp_webpage() {
		
		Driver.getDriver().get(ConfigurationReader.getPropertiesFileValue("dragurl"));
		
	}
	
	@When("User moves image from one window to another")
	public void user_moves_image_from_one_window_to_another() {
		
		WebElement img=Driver.getDriver().findElement(By.xpath("//div[@id='div1']//img[@id='drag1']"));
		
		WebElement wndwTwo=Driver.getDriver().findElement(By.xpath("//div[@id='div2']"));
		
		Actions act=new Actions(Driver.getDriver());
		
		act.dragAndDrop(img, wndwTwo).build().perform();
		
		//act.clickAndHold(img).moveToElement(wndwTwo).release().build().perform();
		
		
	}
	
	@Then("User should see image in another window")
	public void user_should_see_image_in_another_window() {
		
		WebElement imgInWindow=Driver.getDriver().findElement(By.xpath("//div[@id='div2']//img[@id='drag1']"));
		
		if(imgInWindow!=null) {
			
			System.out.println("Drag and Drop performed successfuly");
		}else {
			System.out.println("Drag and Drop FAILED");
		}
		
		
		
	}


}
