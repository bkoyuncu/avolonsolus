@regression
Feature: Perform Scroll Down

Scenario: Open https://testautomationu.applitools.com/cucumber-javascript-tutorial/ and Scroll Down to Chapter 6

Given User get to https://testautomationu.applitools.com/cucumber-javascript-tutorial/
When User perform Scroll Down 
Then User should see Chapter6
