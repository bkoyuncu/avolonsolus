@regression
Feature: Perform Mouse Hover on avalonsolus.com

Scenario Outline: Mouse Hover to Activity element 

Given User get to avalonsolus page and LoggingIn using "<Username>" and "<Password>"
When Performing mouse hovering to Activity
Then User should see Activity menu

 Examples: 
      | Username    | Password      |
      | service_dev | IBecameACoder |
