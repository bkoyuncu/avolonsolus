package com.avalon.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigurationReader {

public static Properties prop;
	
	static {
		
		String propertiesFilePath="C:\\Users\\admin\\eclipse-workspace\\AVALONSOLUS\\src\\test\\resources\\Properties\\propertyfile.properties";
		
		try {
		FileInputStream conf=new FileInputStream(propertiesFilePath);
		prop=new Properties();
		prop.load(conf);
		conf.close();
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static String getPropertiesFileValue(String key) {
		
		return prop.getProperty(key);
		
	}

}
