package com.avalon.stepdefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MouseHoverStepDef {

	@Given("User get to avalonsolus page and LoggingIn using {string} and {string}")
	public void user_get_to_avalonsolus_page_and_LoggingIn_using_and(String string, String string2) {
		
		Driver.getDriver().get(ConfigurationReader.getPropertiesFileValue("url"));
		
		WebElement logInWindow=Driver.getDriver().findElement(By.xpath("//a[@class=\"login js-show-popup\"]"));
		JavascriptExecutor jse=(JavascriptExecutor) Driver.getDriver();
		jse.executeScript("arguments[0].click();", logInWindow);
		
		
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		WebElement usernm=Driver.getDriver().findElement(By.xpath("//input[@placeholder='Username or email']"));
		WebElement pass=Driver.getDriver().findElement(By.xpath("//input[@placeholder='Password']"));
		WebElement loginButton=Driver.getDriver().findElement(By.xpath("//input[@value='Login']"));
		
		jse.executeScript("arguments[0].value='"+string+"';", usernm);
		jse.executeScript("arguments[0].value='"+string2+"';", pass);
		jse.executeScript("arguments[0].click();", loginButton);
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	@When("Performing mouse hovering to Activity")
	public void performing_mouse_hovering_to_Activity() {
		
		WebElement hiddenWindow=Driver.getDriver().findElement(By.xpath("//a[contains(text(),'Howdy')]"));
		WebElement personal=Driver.getDriver().findElement(By.xpath("//a[contains(text(),'Personal')]"));
		WebElement activity=Driver.getDriver().findElement(By.xpath("//li[@id='wp-admin-bar-my-account-activity']"));
		
		Actions act=new Actions(Driver.getDriver());
		act.moveToElement(hiddenWindow).moveToElement(activity).moveToElement(personal).build().perform();
		
		
		
	}
	
	@Then("User should see Activity menu")
	public void user_should_see_Activity_menu() {
		
		WebElement personal=Driver.getDriver().findElement(By.xpath("//a[contains(text(),'Personal')]"));
		
		if(personal!=null) {
			
			System.out.println("Hovering was successful");
			
		}else {
			System.out.println("Hovering FAILED!");
		}
		
	}
	
	

}


