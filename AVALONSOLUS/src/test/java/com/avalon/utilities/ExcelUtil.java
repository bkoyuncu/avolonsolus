package com.avalon.utilities;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {

	public static XSSFWorkbook wrkbk;
	public static XSSFSheet sheet;
	
	public static void excelFileReader() {
		
		try {
		FileInputStream fis=new FileInputStream(ConfigurationReader.getPropertiesFileValue("excelfile"));
		wrkbk=new XSSFWorkbook();
		sheet=wrkbk.getSheetAt(0);
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public String getData(int sheetNumber, int rowNumber, int cellValue) {
		
		sheet=wrkbk.getSheetAt(sheetNumber);
		String data=sheet.getRow(rowNumber).getCell(cellValue).getStringCellValue();
		return data;
		
	}

}
